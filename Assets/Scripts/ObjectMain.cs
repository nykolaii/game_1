using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectMain : MonoBehaviour
{
    [Range (0f, 20f)][SerializeField] private float _speed;
    [SerializeField ]private float _gravity;
    private Rigidbody _rigidbody;
    private Vector3 _vector = Vector3.zero;
    
    public void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
        
    }
    public void Start()
    {
        
    }
    public void Update()
    {
        _vector.x = Input.GetAxis("Horizontal");
        _vector.z = Input.GetAxis("Vertical");
        _vector.y = Input.GetAxis("Jump");
        if (_vector.x == 0 && _vector.z == 0 && _vector.y == 0) return;
        transform.Translate(_vector * _speed * Time.deltaTime);
        
        


    }
}
